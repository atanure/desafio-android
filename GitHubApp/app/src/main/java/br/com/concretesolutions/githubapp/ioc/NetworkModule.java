package br.com.concretesolutions.githubapp.ioc;

import android.content.Context;
import android.net.ConnectivityManager;

import javax.inject.Singleton;

import br.com.concretesolutions.githubapp.rest.IGitHubApiService;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by TANURE-NOTE-DELL on 12/12/2016.
 */

@Module
public class NetworkModule {
    private final Context context;

    public NetworkModule(Context context) {
        this.context = context;
    }

    @Provides
    Context getContext() {
        return this.context;
    }

    @Provides
    ConnectivityManager getConnectivityManager()  {
        return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Provides
    @Singleton
    Retrofit getRetrofitAdapter() {
        return new Retrofit.Builder()
                .baseUrl(IGitHubApiService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
