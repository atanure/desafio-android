package br.com.concretesolutions.githubapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import br.com.concretesolutions.githubapp.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tanure on 10/12/16.
 */

public class RepositoryViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.userPhoto)
    public ImageView userPhoto;

    @BindView(R.id.repositoryName)
    public TextView repositoryName;

    @BindView(R.id.repositoryDescription)
    public TextView repositoryDescription;

    @BindView(R.id.ownerName)
    public TextView ownerName;

    @BindView(R.id.forkIcon)
    public TextView forkIcon;

    @BindView(R.id.starIco)
    public TextView starIco;

    @BindView(R.id.totalForks)
    public TextView totalForks;

    @BindView(R.id.totalStars)
    public TextView totalStars;

    public RepositoryViewHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
    }


}
