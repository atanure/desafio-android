package br.com.concretesolutions.githubapp;


import br.com.concretesolutions.githubapp.ioc.ApplicationModule;
import br.com.concretesolutions.githubapp.ioc.Component;

import br.com.concretesolutions.githubapp.ioc.ApplicationComponent;
import br.com.concretesolutions.githubapp.ioc.DaggerApplicationComponent;
import br.com.concretesolutions.githubapp.ioc.NetworkModule;

/**
 * Created by TANURE-NOTE-DELL on 12/12/2016.
 */

public class Application extends android.app.Application {
    private Component component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule(this.getApplicationContext()))
                .build();
    }

    public Component getComponent() {
        return this.component;
    }
}
