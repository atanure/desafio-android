package br.com.concretesolutions.githubapp.rest;

import java.util.List;

import br.com.concretesolutions.githubapp.domain.Owner;
import br.com.concretesolutions.githubapp.domain.PullRequest;
import br.com.concretesolutions.githubapp.domain.Repositories;
import br.com.concretesolutions.githubapp.domain.Repository;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by tanure on 08/12/16.
 * This is an interface to abstract que Github api access
 */

public interface IGitHubApiService {

    public static final String BASE_URL = "https://api.github.com";

    @GET("search/repositories")
    Call<Repositories> getRepositories(@Query("q") String query, @Query("sort") String sort, @Query("page") int page);

    @GET("users/{username}")
    Call<Owner> getOwner(@Path("username") String username);

    @GET("repos/{user}/{repository}/pulls")
    Call<List<PullRequest>> getPullRequests(@Path("user") String user, @Path("reposotory") String repository, @Query("page") int page);
}
