package br.com.concretesolutions.githubapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by tanure on 10/12/16.
 */

public abstract class GenericViewAdapter<TDataSource, TViewHolder extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<TViewHolder> {

    protected List<TDataSource> dataSource;

    public GenericViewAdapter(List<TDataSource> dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public TViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(this.getDefaultLayout(), parent, false);
        Class<TViewHolder> classViewHolder;
        TViewHolder viewHolder = null;
        // TODO: Tratar os erros

        try {
            viewHolder = (TViewHolder)((Class)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[1]).getConstructor(View.class).newInstance(view);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TViewHolder holder, int position) {
        binderViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    protected abstract int getDefaultLayout();

    protected abstract void binderViewHolder(TViewHolder viewHolder, int position);
}
