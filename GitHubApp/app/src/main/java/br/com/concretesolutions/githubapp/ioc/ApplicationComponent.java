package br.com.concretesolutions.githubapp.ioc;

import javax.inject.Singleton;

import dagger.Module;

/**
 * Created by TANURE-NOTE-DELL on 12/12/2016.
 */

@Singleton
@dagger.Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent extends Component {
}
