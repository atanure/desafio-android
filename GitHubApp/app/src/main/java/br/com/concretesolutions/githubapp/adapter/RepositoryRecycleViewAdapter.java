package br.com.concretesolutions.githubapp.adapter;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;

import br.com.concretesolutions.githubapp.R;
import br.com.concretesolutions.githubapp.domain.Repository;
import br.com.concretesolutions.githubapp.util.FontManager;


/**
 * Created by tanure on 10/12/16.
 */

public class RepositoryRecycleViewAdapter extends GenericViewAdapter<Repository, RepositoryViewHolder> {


    public RepositoryRecycleViewAdapter(List<Repository> dataSource) {
        super(dataSource);
    }

    @Override
    protected int getDefaultLayout() {
        return R.layout.repository_cardview;
    } 

    @Override
    protected void binderViewHolder(RepositoryViewHolder viewHolder, int position) {
        Repository repo = this.dataSource.get(position);
        viewHolder.repositoryName.setText(repo.getName());
        viewHolder.repositoryDescription.setText(repo.getDescription());
        Picasso.with(viewHolder.userPhoto.getContext()).load(repo.getOwner().getUrlAvatar())
                .into(viewHolder.userPhoto);
        viewHolder.ownerName.setText(repo.getOwner().getLogin());

        // Para fontes de ícones
        Typeface iconFont = FontManager.getTypeface(viewHolder.userPhoto.getContext(), FontManager.FONTAWESOME);
        viewHolder.forkIcon.setTypeface(iconFont);
        viewHolder.starIco.setTypeface(iconFont);

        viewHolder.totalForks.setText(String.valueOf(repo.getForksCount()));
        viewHolder.totalStars.setText(String.valueOf(repo.getStarsCount()));
    }
}
