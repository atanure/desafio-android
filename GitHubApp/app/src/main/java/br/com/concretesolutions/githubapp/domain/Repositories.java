package br.com.concretesolutions.githubapp.domain;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by framework on 12/12/16.
 */

public class Repositories {
    @SerializedName("total_count")
    private int totalCount;

    @SerializedName("items")
    private List<Repository> repositories;

    public Repositories() {
        repositories = new ArrayList<Repository>();
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }
}
