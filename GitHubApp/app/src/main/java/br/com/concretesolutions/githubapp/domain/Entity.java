package br.com.concretesolutions.githubapp.domain;

/**
 * Created by tanure on 07/12/16.
 * This class represents the Entity Base of domains
 */

public class Entity {
    private long id;

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return this.id;
    }
}
