package br.com.concretesolutions.githubapp.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tanure on 07/12/16.
 *
 * This class represents an owner of github repository
 */

public class Owner extends Entity{

    @SerializedName("login")
    private String login;

    private String name;

    @SerializedName("avatar_url")
    private String urlAvatar;

    private String blog;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

    public String getUrlAvatar() {
        return this.urlAvatar;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return  this.login;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

}
