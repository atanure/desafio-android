package br.com.concretesolutions.githubapp.ioc;

import javax.inject.Singleton;

import br.com.concretesolutions.githubapp.MainActivity;

/**
 * Created by TANURE-NOTE-DELL on 12/12/2016.
 */

@Singleton
@dagger.Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface Component {
    void inject(MainActivity activity);
}
