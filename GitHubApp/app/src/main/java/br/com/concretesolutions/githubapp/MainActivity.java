package br.com.concretesolutions.githubapp;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import javax.inject.Inject;

import br.com.concretesolutions.githubapp.adapter.RepositoryRecycleViewAdapter;
import br.com.concretesolutions.githubapp.domain.Repositories;
import br.com.concretesolutions.githubapp.domain.Repository;
import br.com.concretesolutions.githubapp.rest.IGitHubApiService;
import br.com.concretesolutions.githubapp.util.FontManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends Activity {

    @BindView(R.id.recycle_view_repositories)
    public RecyclerView recyclerView;

    private RecyclerView.LayoutManager layoutManager;

    private RepositoryRecycleViewAdapter adapter;

    @BindView(R.id.progress_bar)
    public ProgressBar progressBar;

    @Inject
    public Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        ((Application)getApplication()).getComponent().inject(this);

        init();
    }

    private void init() {



        layoutManager = new LinearLayoutManager(this);
        ((LinearLayoutManager)layoutManager).setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        progressBar.setVisibility(View.VISIBLE);

        IGitHubApiService service = retrofit.create(IGitHubApiService.class);

        Call<Repositories> callRepositories  = service.getRepositories("language:java", "stars", 1);

        callRepositories.enqueue(new Callback<Repositories>() {
            @Override
            public void onResponse(Call<Repositories> call, Response<Repositories> response) {
                progressBar.setVisibility(View.GONE);
                Repositories repositories = response.body();
                adapter = new RepositoryRecycleViewAdapter(repositories.getRepositories());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Repositories> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
