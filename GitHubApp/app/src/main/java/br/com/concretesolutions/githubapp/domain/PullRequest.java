package br.com.concretesolutions.githubapp.domain;


/**
 * Created by tanure on 08/12/16.
 * This class represents the pulls of a repository
 */

public class PullRequest extends Entity {

    private String title;

    private String url;

    private String description;

    private Owner user;

    public PullRequest() {
        user = new Owner();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Owner getUser() {
        return user;
    }

    public void setUser(Owner user) {
        this.user = user;
    }
}
